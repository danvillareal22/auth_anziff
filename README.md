Custom built webservice and authetication plugin for ANZIIF.

The plugin uses SSO to autheticate users and SOAP for webservice.

Web service plugin for ANZIIF (Moodle 3.2.X)
------------------------------------------
It is an authentication plugin at the same time it is a webservice plugin that enable ANZIIF to:
1. Authenticate user.
2. Create user.
3. Update user.
4. Restore a course from a template course and modify time and date of a specific module.
5. Receiving of grades from CRM to Moodle.
6. Allocate marker to assessment activity(Assignment).
7. Send Grade from moodle to CRM using adhoc task and webservice request.

Developed by: Daniel D. Villareal and Jake Naughton