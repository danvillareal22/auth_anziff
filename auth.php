<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Anobody can login with any password.
 *
 * @package auth_anziif
 * @author Jake Naughton
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * Plugin for no authentication.
 */
class auth_plugin_anziif extends auth_plugin_base {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->authtype = 'anziif';
        $this->config = get_config('auth/anziif');
    }

    /**
     * Old syntax of class constructor. Deprecated in PHP7.
     *
     * @deprecated since Moodle 3.1
     */
    public function auth_plugin_anziif() {
        debugging('Use of class name as constructor is deprecated', DEBUG_DEVELOPER);
        self::__construct();
    }

    /**
     * Returns true if the username and password work or don't exist and false
     * if the user exists and the password is wrong.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login ($username, $password) {
        return false;
    }

    public function pre_loginpage_hook() {
        global $DB, $USER, $CFG, $SESSION;

        if($this->config->auth_activated == 0){
            return;
        }

        $incoming_url = $SESSION->wantsurl;

        // Check for token
        $token = optional_param('t', 0, PARAM_ALPHANUMEXT);
        if($token) {
            // catch
            $returned_token = $this->fetch_token($token);
            if($returned_token->SystemError == "Token invalid") {
                redirect('https://anziif.com/?systemerror=' . $returned_token->SystemError);
            } else if ($returned_token->SystemError == NULL){

                if(!$returned_token->UserDetails->MasterId){
                    print_error('No Master Id in token');
                }

                $idnumber = $returned_token->UserDetails->MasterId;
                $sql = 'SELECT * FROM mdl_user WHERE idnumber = ?';

                try {
                    $user_to_login = $DB->get_records_sql($sql,array($idnumber));
                } catch (Exception $e) {
                    print_error('Error with Moodle function');
                }
                $number_of_users = count($user_to_login);

                if($number_of_users == 0) {
                    print_error('No users exist in Moodle with the Master ID passed by the Token');
                } else if ($number_of_users > 1) {
                    print_error('Multiple users detected in the system with the same Master ID passed by the Token');
                } else if ($number_of_users == 1) {
                    $user_to_login = reset($user_to_login);
                    // Log User in
                    $user = get_complete_user_data('username', $user_to_login->username);
                    complete_user_login($user);
                    $SESSION->usertoken = $token;
                    redirect($incoming_url);
                    exit;
                } else {      
                    print_error('Error with Moodle function');
                }

            } else {
                redirect('https://anziif.com/?systemerror='.$returned_token->SystemError);
            }

            // else if ( $returned_token->SystemError == 'Token Not Found' ){ 

            //     if(@$returned_token->FiendlyError) {
            //         print_error($returned_token->FiendlyError);
            //     } else {
            //         print_error('The users session has expired or has already logged out');
            //     }
            // } else {
            //     print_error('Unknown behaviour with Token');
            // }
        } else {
            // redirect to Single Sign Off
            redirect($this->config->loginlink);
        }

        $this->loginpage_hook();
    }

    public function loginpage_hook() {
        global $DB, $USER, $CFG, $SESSION;

        if($this->config->auth_activated == 0){
            return;
        }
        $redirect = optional_param('redirect', 1, PARAM_INT);
        if($redirect == 0 || isset($SESSION->nologinredirect) ) {
            $SESSION->nologinredirect = true;
            return;
        }

        // Check for token
        $token = optional_param('t', 0, PARAM_ALPHANUMEXT);
        if($token) {
            // catch
            $returned_token = $this->fetch_token($token);
            if($returned_token->SystemError == "Token invalid") {
                redirect('https://anziif.com/?systemerror=' . $returned_token->SystemError);
            } else if ($returned_token->SystemError == NULL){

                if(!$returned_token->UserDetails->MasterId){
                    print_error('No Master Id in token');
                }

                $idnumber = $returned_token->UserDetails->MasterId;
                $sql = 'SELECT * FROM mdl_user WHERE idnumber = ?';

                try {
                    $user_to_login = $DB->get_records_sql($sql,array($idnumber));
                } catch (Exception $e) {
                    print_error('Error with Moodle function');
                }
                $number_of_users = count($user_to_login);

                if($number_of_users == 0) {
                    print_error('No users exist in Moodle with the Master ID passed by the Token');
                } else if ($number_of_users > 1) {
                    print_error('Multiple users detected in the system with the same Master ID passed by the Token');
                } else if ($number_of_users == 1) {
                    $user_to_login = reset($user_to_login);
                    // Log User in
                    $user = get_complete_user_data('username', $user_to_login->username);
                    complete_user_login($user);
                    $SESSION->usertoken = $token;
                    // $USER->loggedin = true;
                    // $USER->site = $CFG->wwwroot;
                    // set_moodle_cookie($USER->username);
                    redirect($CFG->wwwroot);
                    exit;
                } else {      
                    print_error('Error with Moodle function');
                }

            } else {
                redirect('https://anziif.com/?systemerror='.$returned_token->SystemError);
            }

            // else if ( $returned_token->SystemError == 'Token Not Found' ){ 

            //     if(@$returned_token->FiendlyError) {
            //         print_error($returned_token->FiendlyError);
            //     } else {
            //         print_error('The users session has expired or has already logged out');
            //     }
            // } else {
            //     print_error('Unknown behaviour with Token');
            // }
        } else {
            // redirect to Single Sign Off
            redirect($this->config->loginlink);
        }

    }

    public function logoutpage_hook() {
        global $SESSION;

        if(@$SESSION->usertoken) {
            $url = 'https://anziif.com/services/SsoService.svc/Logout?token=' . $SESSION->usertoken;
            require_logout();
            $logout_data = file_get_contents($url);
            redirect($this->config->logoutlink);
        }

        return;
    }



    public function fetch_token($token) {

        $token_url = $this->config->tokenlink . $token;
        $response = file_get_contents($token_url);
        $json_object = json_decode($response);

        return $json_object;
    }

    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object
     * @param  string  $newpassword Plaintext password
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        // This will also update the stored hash to the latest algorithm
        // if the existing hash is using an out-of-date algorithm (or the
        // legacy md5 algorithm).
        return update_internal_user_password($user, $newpassword);
    }

    function prevent_local_passwords() {
        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null;
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return false;
    }

    /**
     * Returns true if plugin can be manually set.
     *
     * @return bool
     */
    function can_be_manually_set() {
        return true;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        // Defaults
        if (!isset($config->auth_activated)) {
            $config->auth_activated = 0;
        }
        if (!isset($config->loginlink)) {
            $config->loginlink = '';
        }
        if (!isset($config->tokenlink)) {
            $config->tokenlink = '';
        }
        if (!isset($config->logoutlink)) {
            $config->logoutlink = '';
        }

        if (!isset($config->assessment_duration)) {
            $config->assessment_duration = 1;
        }
        if (!isset($config->crmurlapi)) {
            $config->crmurlapi = 'https://wst.anziif.com:8080/CYTYCWebServices/ExternalService/ExternalService.svc?wsdl';
        }
        if (!isset($config->crmtokenapi)) {
            $config->crmtokenapi = '3bbcee75cecc5b568031';
        }
        if (!isset($config->emailforerror)) {
            $config->emailforerror = '';
        }

        // Save Settings
        set_config('auth_activated',$config->auth_activated,'auth/anziif');
        set_config('loginlink',     $config->loginlink,     'auth/anziif');
        set_config('tokenlink',     $config->tokenlink,     'auth/anziif');
        set_config('logoutlink',    $config->logoutlink,    'auth/anziif');

        //webservice
        set_config('assessment_duration',    $config->assessment_duration,    'auth/anziif');
        set_config('crmurlapi',    $config->crmurlapi,    'auth/anziif');
        set_config('crmtokenapi',    $config->crmtokenapi,    'auth/anziif');
        set_config('emailforerror',    $config->emailforerror,    'auth/anziif');
        return true;
    }

}


