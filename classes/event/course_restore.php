<?php

namespace auth_anziif\event;
defined('MOODLE_INTERNAL') || die();

class course_restore extends \core\event\base {
 
   protected function init() {
        $this->data['crud'] = 'c'; // c(reate), r(ead), u(pdate), d(elete)
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'course_restore';
    }
 
    public static function get_name() {
        return 'Course Restoration';
    }
 
    public function get_description() {
        return $this->other['description'];
    }

 
    public function get_legacy_logdata() {
        // Override if you are migrating an add_to_log() call.
        return array($this->courseid, 'auth_anziif', 'Course Restoration',
            $this->description,
            $this->objectid, $this->context->instanceid);
    }
 
}