<?php
namespace auth_anziif;
defined('MOODLE_INTERNAL') || die();

class observer {
    public static function send_grade_event(\core\event\user_graded $event) {
        global $CFG, $COURSE, $DB;
        $eventdata = $event->get_data();

        $gg = $DB->get_record_sql("SELECT * FROM mdl_grade_items WHERE courseid = ? AND id = ?",array($COURSE->id,$eventdata['other']['itemid'])); 
        $coursemodule = $DB->get_record_sql("SELECT CM.* FROM mdl_course_modules CM JOIN mdl_modules M ON M.id = CM.module WHERE CM.instance = ? AND CM.course = ? AND M.name = ?",array($gg->iteminstance, $COURSE->id, $gg->itemmodule));
        $user = $DB->get_record('user',array('id'=> (int) $eventdata['relateduserid']));

        $task = new \auth_anziif\task\send_grade();
		$task->set_custom_data(array(
           'objectdata' => $eventdata,
           'courseidnumber' => $COURSE->idnumber,
           'masterid' => $user->username,
           'activityidnumber' => $coursemodule,
           'enrolment'=>'111',
           'grade_item' =>$gg   
	    ));
        \core\task\manager::queue_adhoc_task($task);

    }
}