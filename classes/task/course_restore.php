<?php

namespace auth_anziif\task;

class course_restore extends \core\task\adhoc_task
{
    public function get_component() {
        return 'auth_anziif';
    }
    public function execute() {
        global $CFG;
        require_once($CFG->dirroot . '/auth/anziif/locallib.php');
		
				
		mtrace("Course is backing up: Template ID: ".$this->get_custom_data()->templateid);
		
		$backupid = backup_template($this->get_custom_data()->templateid);

		mtrace("Webservice is creating a temp course");
		
		$course = create_new_course($this->get_custom_data()->categorypath,$this->get_custom_data()->coursefullname,$this->get_custom_data()->courseshortname,$this->get_custom_data()->startdate,$this->get_custom_data()->enddate);
		if($course == false){
			mtrace("Course already exist will stop the backup and restore");
			return true;
		}

	    mtrace("Course is starting to restore: Course ID: ".$course->id);

	    restore_to_course($course->id, $backupid);
		
		updatecourse($course->id, $this->get_custom_data()->categorypath,$this->get_custom_data()->coursefullname,$this->get_custom_data()->courseshortname,$this->get_custom_data()->startdate,$this->get_custom_data()->enddate);

		mtrace("Course successfully restored: Course ID: ".$course->id);
		
		mtrace('Setting Course assessment dates');

		$quiz = set_assessment_date($course->id,$this->get_custom_data()->assessmentdate);

		mtrace('Course restoration finished!');
		return true;
    }
	
}