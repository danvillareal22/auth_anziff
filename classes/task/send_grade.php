<?php
namespace auth_anziif\task;

class send_grade extends \core\task\adhoc_task
{
	public function get_component() {
        return 'auth_anziif';
    }
	
    public function execute() {
        global $CFG;
		require_once($CFG->dirroot . '/auth/anziif/locallib.php');
		sendgrade($this->get_custom_data()->objectdata,$this->get_custom_data()->courseidnumber,$this->get_custom_data()->masterid,$this->get_custom_data()->activityidnumber,$this->get_custom_data()->enrolment,$this->get_custom_data()->grade_item);
		
		return true;
	}
}