<?php

$observers = array(
    array(
        'eventname'   => '\core\event\user_graded',
        'callback'    => '\auth_anziif\observer::send_grade_event',
    )
);