<?php
$functions = array(
		'auth_anziif_create_user' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'create_user',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Create user based on anziif database',
                'type'        => 'write',
        ),
		'auth_anziif_update_user' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'update_user',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Update user based on anziif database',
                'type'        => 'write',
        ),
		'auth_anziif_course_restore' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'course_restore',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Course restoration',
                'type'        => 'write',
        ),
		'auth_anziif_send_grade' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'send_grade',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Receive Grade from CRM',
                'type'        => 'read',
        ),
		'auth_anziif_receive_grade' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'receive_grade',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Receive Grade from CRM',
                'type'        => 'write',
        ),
		'auth_anziif_allocate_marker' => array(
                'classname'   => 'auth_anziif_external',
                'methodname'  => 'allocate_marker',
                'classpath'   => 'auth/anziif/externallib.php',
                'description' => 'Allocate Marker to Assignment',
                'type'        => 'write',
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Anziif' => array(
                'functions' => array ('auth_anziif_create_user',
				      'auth_anziif_update_user',
				      'auth_anziif_course_restore',
				      'auth_anziif_send_grade',
				      'auth_anziif_receive_grade',
				      'auth_anziif_allocate_marker'),
                'restrictedusers' => 0,
		'shortname'=>'anziifwebservice',
                'enabled'=>1,
        )
);
