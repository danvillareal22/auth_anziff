<?php
require_once($CFG->libdir . "/externallib.php");

class auth_anziif_external extends external_api {

    //create user
	public static function create_user_parameters() {
        return new external_function_parameters(
                array('masterid' => new external_value(PARAM_TEXT, 'masterid', VALUE_DEFAULT, null),
					  'suspended' => new external_value(PARAM_INT, 'suspended', VALUE_DEFAULT, null),
					  'firstname' => new external_value(PARAM_TEXT, 'firstname', VALUE_DEFAULT, null),
					  'lastname' => new external_value(PARAM_TEXT, 'lastname', VALUE_DEFAULT, null),
					  'email' => new external_value(PARAM_EMAIL, 'email', VALUE_DEFAULT, null),
					  'country' => new external_value(PARAM_TEXT, 'email', VALUE_DEFAULT, null))
        );
    }
	
	public static function create_user($masterid = null, $suspended = null, $firstname = null, $lastname = null, $email = null, $country = null){
		global $DB, $CFG;
		require_once($CFG->dirroot . "/user/lib.php");
		
		$response = array();
		// $sql = "SELECT * FROM mdl_user U WHERE U.username = ? OR U.email = ? OR U.idnumber = ?";
		// $userexist = $DB->get_records_sql($sql, array($email, $email, $masterid));
		$userexist = $DB->get_record('user',array('username'=>$masterid));
			if($userexist){
				$response[] = array(
					'statuscode' => 400,
					'statusmessage' => 'User already exist, please check your master user id.',
					'moodle_userid' => $userexist->id,
				);
			}else{
				
				$emailexist = $DB->get_records_sql("SELECT * FROM mdl_user WHERE username != ? AND email = ?", array($masterid,$email));
					if($emailexist){
						$response[] = array(
							'statuscode' => 400,
							'statusmessage' => 'Email address is already in use by another user, please user another email address.',
							'moodle_userid' => null,
						);
						$result = array(
							'responses' => $response
						);
						
						return $result;
						
					}
				
				$user = new stdClass();
				$user->idnumber = (string)$masterid;
				$user->suspended = $suspended;
				$user->firstname = $firstname;
				$user->lastname = $lastname;
				$user->email = $email;
				$user->username = (string)$masterid;
				$user->mnethostid = 1;
				$user->confirmed = 1;
				$user->auth = 'anziif';
				$user->country = $country;
							
				$newuser = user_create_user($user, false);

				$response[] = array(
					'statuscode' => 200,
					'statusmessage' => 'User successfully created!',
					'moodle_userid' => $newuser,
				);
				
			}
		
		$result = array(
            'responses' => $response
        );

        return $result;
	}
	
	public static function create_user_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
	//update user
	public static function update_user_parameters() {
        return new external_function_parameters(
                array('masterid' => new external_value(PARAM_TEXT, 'masterid', VALUE_DEFAULT, null),
					  'suspended' => new external_value(PARAM_INT, 'suspended', VALUE_DEFAULT, null),
					  'firstname' => new external_value(PARAM_TEXT, 'firstname', VALUE_DEFAULT, null),
					  'lastname' => new external_value(PARAM_TEXT, 'lastname', VALUE_DEFAULT, null),
					  'email' => new external_value(PARAM_EMAIL, 'email', VALUE_DEFAULT, null),
					  'country' => new external_value(PARAM_TEXT, 'email', VALUE_DEFAULT, null))
        );
    }
	
	public static function update_user($masterid = null, $suspended = null, $firstname = null, $lastname = null, $email = null, $country= null){
		global $DB, $CFG;
		require_once($CFG->dirroot . "/user/lib.php");
		
		$response = array();
		$user = $DB->get_record('user',array('username' => $masterid));
			if($user){
				if($user->email == $email){
					$user->idnumber = (string)$masterid;
					$user->suspended = $suspended;
					$user->firstname = $firstname;
					$user->lastname = $lastname;
					$user->email = $email;
					$user->username = (string)$masterid;
					$user->auth = 'anziif';
					$user->country = $country;
					
					user_update_user($user, false);

					$response[] = array(
						'statuscode' => 200,
						'statusmessage' => 'User account successfully updated!',
						'moodle_userid' => $user->id,
					);
				}else{
					
					$emailexist = $DB->get_records_sql("SELECT * FROM mdl_user WHERE username != ? AND email = ?", array($masterid,$email));
						if($emailexist){
							$response[] = array(
								'statuscode' => 400,
								'statusmessage' => 'Email address is already in use by another user, please user another email address.',
								'moodle_userid' => null,
							);
							$result = array(
								'responses' => $response
							);
			
							return $result;
						}
							
					$user->idnumber = (string)$masterid;
					$user->suspended = $suspended;
					$user->firstname = $firstname;
					$user->lastname = $lastname;
					$user->email = $email;
					$user->username = (string)$masterid;
					$user->auth = 'anziif';
					$user->country = $country;
							
					user_update_user($user, false);

					$response[] = array(
						'statuscode' => 200,
						'statusmessage' => 'User account successfully updated!',
						'moodle_userid' => $user->id,
					);
								
				}
				
			}else{
				
				$response[] = array(
					'statuscode' => 404,
					'statusmessage' => 'Failed to find user in the system, please check your master user id!',
					'moodle_userid' => null,
				);
				
			}
		
		$result = array(
            'responses' => $response
        );

        return $result;
		
	}
	
	public static function update_user_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
	//course restoration
	public static function course_restore_parameters() {
        return new external_function_parameters(
                array('categorypath' => new external_value(PARAM_TEXT, 'categorypath', VALUE_DEFAULT, null),
					  'templateid' => new external_value(PARAM_TEXT, 'templateid', VALUE_DEFAULT, null),
					  'coursefullname' => new external_value(PARAM_TEXT, 'coursefullname', VALUE_DEFAULT, null),
					  'courseshortname' => new external_value(PARAM_TEXT, 'courseshortname', VALUE_DEFAULT, null),
					  'startdate' => new external_value(PARAM_INT, 'startdate', VALUE_DEFAULT, null),
					  'enddate' => new external_value(PARAM_INT, 'enddate', VALUE_DEFAULT, null),
					  'assessmentdate' => new external_value(PARAM_INT, 'assessmentdate', VALUE_DEFAULT, null),
					  )
        );
    }

	
	public static function course_restore($categorypath = null, $templateid = null, $coursefullname = null, $courseshortname = null, $startdate = null, $enddate = null, $assessmentdate = null){
		global $DB;
		
		$templateid = $DB->get_record('course',array('idnumber'=>$templateid));
		if(!$templateid){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Course '.$templateid.' doesnt exist on the database. Please check your template id number(course to be backup)',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$coursecat = $DB->get_record('course_categories',array('id' => $categorypath));
		if(!$coursecat){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Course category'.$categorypath.' doesnt exist on the database. Please check your course category id (course category id)',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}

		$csn = $DB->get_record('course',array('shortname' => $courseshortname));
		if($csn){
			$response[] = array(
				'statuscode' => 400,
				'statusmessage' => 'Course shortanme: '.$courseshortname.' already exist. Please check your course shortname.',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		$adhoc = $DB->get_records_sql("SELECT * FROM mdl_task_adhoc WHERE classname = ?",array('\auth_anziif\task\course_restore'));
		if($adhoc){
			foreach($adhoc as $a){
				if(json_decode($a->customdata)->courseshortname == $courseshortname){
					$response[] = array(
						'statuscode' => 400,
						'statusmessage' => 'Course shortanme: '.$courseshortname.' already exist in the queue(Adhoc Task). Please check your course shortname.',
						'moodle_userid' => null,
					);
					$result = array(
						'responses' => $response
					);
				
					return $result;
				}
			}
		}
		
		$task = new \auth_anziif\task\course_restore();
		$task->set_custom_data(array(
		   'categorypath' => $categorypath,
		   'templateid' => $templateid->id,
		   'coursefullname' => $coursefullname,
		   'courseshortname' => $courseshortname,
		   'startdate' => $startdate,
		   'enddate' => $enddate,
		   'assessmentdate' => $assessmentdate		   
	    ));
		\core\task\manager::queue_adhoc_task($task);

		$response = array();
		$response[] = array(
			'statuscode' => 200,
			'statusmessage' => 'Course restoration was added to the background task!',
			'moodle_userid' => null,
		);
		
		$result = array(
            'responses' => $response
        );
		
		return $result;
	}
	
	public static function course_restore_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
	//send grade function
	public static function send_grade_parameters() {
        return new external_function_parameters(
                array('masterid' => new external_value(PARAM_INT, 'masterid', VALUE_DEFAULT, null),
					  'courseidnumber' => new external_value(PARAM_TEXT, 'courseidnumber', VALUE_DEFAULT, null),
					  'activityidnumber' => new external_value(PARAM_TEXT, 'activityidnumber', VALUE_DEFAULT, null),
					  'timeupdated' => new external_value(PARAM_INT, 'timeupdated', VALUE_DEFAULT, null),
					  'grade' => new external_value(PARAM_INT, 'grade', VALUE_DEFAULT, null))
        );
    }
	
	public static function send_grade($masterid = null, $courseidnumber = null, $activityidnumber = null, $timeupdated = null, $grade = null){
		global $DB, $CFG;

		$response = array();
		$anziifconfig = get_config('auth/anziif');
		
		$user = $DB->get_record('user',array('username'=>$masterid));
		if(!$user){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'User '.$masterid.'not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$course = $DB->get_record('course',array('idnumber'=>$courseidnumber));
		if(!$course){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Course '.$courseidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$coursemodule = $DB->get_record('course_modules',array('idnumber'=>$activityidnumber));
		if(!$coursemodule){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Activity number '.$activityidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$modinfo = get_fast_modinfo($course->id);
		$cm = $modinfo->get_cm($coursemodule->id);

		$gradeitem = $DB->get_record('grade_items',array('courseid'=>$course->id, 'iteminstance'=>$cm->instance, 'itemname'=>$cm->name));
			
		if($gradeitem){
			$gg = $DB->get_record_sql('SELECT GG.* FROM mdl_grade_grades GG JOIN mdl_user U ON U.id = GG.userid WHERE U.idnumber = ? AND GG.itemid = ?',array($masterid, $gradeitem->id));

			if($gg){
				// $grade = var_export($gg,true);
				//view user grade and send to api
				// $myfile = fopen($CFG->dirroot ."/auth/anziif/usergraded.txt", "w+") or die("Unable to open file!");
				// fwrite($myfile, $grade);
				// fclose($myfile);
				//$enrolment = $DB->get_record_sql('SELECT UE.* FROM mdl_user_enrolments UE JOIN mdl_enrol E ON E.id = UE.enrolid WHERE UE.userid = ? AND E.courseid = ?',array($user->id,$course->id));
				//$enrolment = $DB->get_record('user_enrolments',array('userid'=>$user->id, 'course'=>$course->id));
				// var_dump($enrolment);
				$requestParams = array(
					'assessmentResult' => array(
						'MasterId' => $masterid,
						'CourseId' => $courseidnumber,
						'ActivityId' => $activityidnumber,
						
						'Result' => $gg->finalgrade,
						'ResultDate' => date('Y-m-d\TH:i:s.u', $gg->timemodified)
					),
					'wstoken' => $anziifconfig->crmtokenapi
				);
				$client = new SoapClient($anziifconfig->crmurlapi);
				try{
					$apiresponse = $client->SetAssessmentResult($requestParams);
				}catch(Exception $e){
				
					if($e->getMessage()){
						
						$response[] = array(
							'statuscode' => 400,
							'statusmessage' => $e->getMessage(),
							'moodle_userid' => $user->id,
						);

						$result = array(
							'responses' => $response
						);
							
						return $result;
					}
										
				}

				$response[] = array(
					'statuscode' => 200,
					'statusmessage' => "User grade has been sent on ANZIIF CRM API. Activity: ".$activityidnumber. " is now processing.",
					'moodle_userid' => $user->id,
				);
			}else{
					
				$response[] = array(
					'statuscode' => 400,
					'statusmessage' => "The user has no grade on Activity number: ".$activityidnumber,
					'moodle_userid' => $user->id,
				);
			}
		}else{
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Activity number: '.$activityidnumber.' not found',
				'moodle_userid' => $user->id,
			);
		}

		$result = array(
			'responses' => $response
		);
			
		return $result;
		
	}
	
	public static function send_grade_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
	//recieve grade function
	public static function receive_grade_parameters() {
        return new external_function_parameters(
                array('masterid' => new external_value(PARAM_INT, 'masterid', VALUE_DEFAULT, null),
					  'courseidnumber' => new external_value(PARAM_TEXT, 'courseidnumber', VALUE_DEFAULT, null),
					  'activityidnumber' => new external_value(PARAM_TEXT, 'activityidnumber', VALUE_DEFAULT, null),
					  'timeupdated' => new external_value(PARAM_INT, 'timeupdated', VALUE_DEFAULT, null),
					  'grade' => new external_value(PARAM_INT, 'grade', VALUE_DEFAULT, null))
        );
    }
	
	
	public static function receive_grade($masterid = null, $courseidnumber = null, $activityidnumber = null, $timeupdated = null, $grade = null){
		global $DB, $CFG;

		$response = array();
		$user = $DB->get_record('user',array('username'=>$masterid));
		if(!$user){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'User '.$masterid.'not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$course = $DB->get_record('course',array('idnumber'=>$courseidnumber));
		if(!$course){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Course '.$courseidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$coursemodule = $DB->get_record('course_modules',array('idnumber'=>$activityidnumber));
		if(!$coursemodule){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Activity number '.$activityidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$modinfo = get_fast_modinfo($course->id);
		$cm = $modinfo->get_cm($coursemodule->id);

		$gradeitem = $DB->get_record('grade_items',array('courseid'=>$course->id, 'iteminstance'=>$cm->instance, 'itemname'=>$cm->name));
			
		if($gradeitem){
			$gg = $DB->get_record_sql('SELECT GG.* FROM mdl_grade_grades GG JOIN mdl_user U ON U.id = GG.userid WHERE U.idnumber = ? AND GG.itemid = ?',array($masterid, $gradeitem->id));
	
			if($gg){
				$gg->finalgrade = $grade;
				$gg->feedback = 'auth/anziif';
				$gg->feedbackformat = 1;
				$gg->overridden = 1;
				$gg->timemodified = $timeupdated;
				$DB->update_record('grade_grades', $gg);
					
				$response[] = array(
					'statuscode' => 200,
					'statusmessage' => 'Grading successfully for activity number: '.$activityidnumber,
					'moodle_userid' => $user->id,
				);
					
			}else{
					
				$response[] = array(
					'statuscode' => 400,
					'statusmessage' => "The user has no grade on Activity number: ".$activityidnumber,
					'moodle_userid' => $user->id,
				);
			}
		}else{
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Activity number: '.$activityidnumber.' not found',
				'moodle_userid' => $user->id,
			);
		}
		
		
		$result = array(
			'responses' => $response
		);
			
		return $result;
		
	}
	
	public static function receive_grade_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
	//allocate marker function
	public static function allocate_marker_parameters() {
        return new external_function_parameters(
                array('masterid' => new external_value(PARAM_INT, 'masterid', VALUE_DEFAULT, null),
					  'courseidnumber' => new external_value(PARAM_TEXT, 'courseidnumber', VALUE_DEFAULT, null),
					  'activityidnumber' => new external_value(PARAM_TEXT, 'activityidnumber', VALUE_DEFAULT, null),
					  'markerid' => new external_value(PARAM_INT, 'markerid', VALUE_DEFAULT, null))
        );
    }
	
	public static function allocate_marker($masterid = null, $courseidnumber = null, $activityidnumber = null, $markerid = null){
		global $DB, $CFG;
		$admin = get_admin();
		$course = $DB->get_record('course',array('idnumber' => $courseidnumber));
		if(!$course){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Course '.$courseidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$coursecontext = context_course::instance($course->id);
		
		$teacher = $DB->get_record('user',array('id' => $markerid));
		
		if(!$teacher){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => "Marker not found on the system",
				'moodle_userid' => null,
			);
				
			$result = array(
				'responses' => $response
			);
					
			return $result;
		}
		$student = $DB->get_record('user',array('username' => $masterid));
		if(!$student){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => "Student not found on the system",
				'moodle_userid' => null,
			);
				
			$result = array(
				'responses' => $response
			);
					
			return $result;
		}
		
		if(!is_enrolled($coursecontext, $teacher->id, 'moodle/course:manageactivities')){
			$enrol = $DB->get_record('enrol',array('enrol'=>'manual','courseid'=>$course->id));
			$context = context_course::instance($course->id, MUST_EXIST);

			$ue = new stdClass();
            $ue->enrolid      = $enrol->id;
            $ue->status       = 0;
            $ue->userid       = $teacher->id;
            $ue->timestart    = strtotime("NOW");
            $ue->timeend      = 0;
            $ue->modifierid   = $admin->id;
            $ue->timecreated  = time();
            $ue->timemodified = strtotime("NOW");
			$ue->id = $DB->insert_record('user_enrolments', $ue);

			role_assign(3, $teacher->id, $context->id);
		}
		
		if(!is_enrolled($coursecontext, $student->id)){
			
			$response[] = array(
				'statuscode' => 400,
				'statusmessage' => "Student not enrolled on the course",
				'moodle_userid' => $student->id,
			);
			
			$result = array(
				'responses' => $response
			);
					
			return $result;
			
		}
		
		$coursemodule = $DB->get_record('course_modules',array('idnumber'=>$activityidnumber,'module'=>1));
		if(!$coursemodule){
			$response[] = array(
				'statuscode' => 404,
				'statusmessage' => 'Assignment activity number '.$activityidnumber.' not found on the database',
				'moodle_userid' => null,
			);
			$result = array(
				'responses' => $response
			);
		
			return $result;
		}
		
		$em = $DB->get_record('assign_user_flags', array('userid' => $student->id, 'assignment' => $coursemodule->instance));
		if($em){
			$em->allocatedmarker = $teacher->id;
			$DB->update_record('assign_user_flags',$em);
			$response[] = array(
				'statuscode' => 200,
				'statusmessage' => 'User marker updated',
				'moodle_userid' => $em->userid,
			);

		}else{
			$marker = new stdClass();
			$marker->userid = $student->id;
			$marker->assignment = $coursemodule->instance;
			$marker->locked = 0;
			$marker->mailed = 0;
			$marker->extensionduedate = 0;
			$marker->allocatedmarker = $teacher->id;
			$DB->insert_record('assign_user_flags',$marker);
			$response[] = array(
				'statuscode' => 200,
				'statusmessage' => 'User marker updated',
				'moodle_userid' => $student->id,
			);
		}
		
		
		$result = array(
			'responses' => $response
		);
			
		return $result;
	}
	
	public static function allocate_marker_returns() {
        return new external_single_structure(
            array(
                'responses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'statuscode' => new external_value(PARAM_INT, 'statuscode'),
                            'statusmessage' => new external_value(PARAM_TEXT, 'statusmessage'),
                            'moodle_userid' => new external_value(PARAM_INT, 'moodle_userid'),
                        )
                    )
                ),
            )
        );
    }
	
}
