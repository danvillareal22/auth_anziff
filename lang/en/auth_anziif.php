<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_anziif', language 'en'.
 *
 * @package   auth_anziif
 * @copyright 2017 Jake Naughton and Daniel Villareal
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['auth_anziifdescription'] = 'Anziif webservice and authetication';
$string['pluginname'] = 'Anziif';
$string['settings_title'] = 'Anziif';
$string['login_link'] = 'Login URL';
$string['login_link_description'] = 'Set the URL where users are redirected to login.';
$string['token_link'] = 'Token URL';
$string['token_link_description'] = 'The Token is added to the end of this URL to confirm the user is valid';
$string['auth_activated'] = 'Turn on SSO';
$string['auth_activated_description'] = 'Turn on Single Sign On';
$string['logout_link'] = 'Log out link';
$string['logout_link_description'] = 'This is where users are redirected when they have logged out';

//webservice
$string['webservice_anziif'] = 'Webservice Configuration';
$string['webservice_anziif_assessment_duration'] = 'Assessment Duration';
$string['webservice_anziif_assessment_duration_desc'] = 'Duration day for the assessment(Number of days).';
$string['webservice_anziif_crmurlapi'] = 'CRM URL API';
$string['webservice_anziif_crmurlapi_desc'] = 'CRM URL for sending of grade.';
$string['webservice_anziif_crmtokenapi'] = 'CRM Token API';
$string['webservice_anziif_crmtokenapi_desc'] = 'CRM Token for sending of grade.';
$string['webservice_anziif_emailforerror'] = 'Email Address';
$string['webservice_anziif_emailforerror_desc'] = 'Error logs will be sent to this email address';
