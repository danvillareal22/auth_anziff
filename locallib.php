<?php

require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot . '/backup/moodle2/backup_plan_builder.class.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot . '/backup/util/ui/import_extensions.php');
require_once($CFG->dirroot . '/course/lib.php');

//function create coursecategory
function create_new_course($categorypath,$coursefullname,$courseshortname,$startdate = null, $enddate = null){
	global $DB, $CFG;
	require_once($CFG->libdir . '/coursecatlib.php');
	mtrace("Creating new course from template");
	$course = new stdClass();
	$course->fullname = $coursefullname;
	$course->shortname = $courseshortname;
	$course->idnumber = $courseshortname;
    $course->category = $categorypath;
    $course->startdate = $startdate;
	try{
		$course = create_course($course);
	}catch(Exception $e){
        $event = \auth_anziif\event\course_restore::create(array(
            'objectid' => null,
            'courseid' => 1,
            'context' => context_system::instance(),
            'other' => array(
                'description' => $e->getMessage()
            )

        ));
        $event->trigger();
		mtrace($e->getMessage());
		return false;
	}
	mtrace("New course successfully created and restored data from template course!");
	return $course;
}

function set_assessment_date($course,$assessmentdate){
    global $DB;

    $anziifconfig = get_config('auth/anziif');
    
    $modinfo = get_fast_modinfo($course);
    $section = $modinfo->get_section_info_all();
    foreach($section as $s){
        if(($s->name == 'Exam') || ($s->name == 'Exams')){
            foreach ($modinfo->sections[$s->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];
                if($mod->modname == 'quiz'){
                    var_dump($mod->instance);
                    $quiz = $DB->get_record('quiz',array('id'=>$mod->instance));
                    $quiz->timeopen = $assessmentdate;
                    $quiz->timeclose = strtotime('+'.$anziifconfig->assessment_duration.' day', $quiz->timeopen);
                    $DB->update_record('quiz',$quiz);
                }
    
            }
    
        }
    }
}

//function for backup a template course
function backup_template($templateid) {
	
	$admin = get_admin();
	$config = get_config('backup');
	
	$settings = array(
		
	    //'users' => 'backup_auto_users',
		'activities' => 'backup_auto_activities',
		'blocks' => 'backup_auto_blocks',
		'badges' => 'backup_auto_badges',
		'role_assignments' => 'backup_auto_role_assignments',
		'filters' => 'backup_auto_filters',
		'comments' => 'backup_auto_comments',
		'completion_information' => 'backup_auto_userscompletion',
		//'logs' => 'backup_auto_logs',
		//'histories' => 'backup_auto_histories',
		'questionbank' => 'backup_auto_questionbank',
        'groups' => 'backup_auto_groups'
	);
    $bc = new backup_controller(backup::TYPE_1COURSE, $templateid, backup::FORMAT_MOODLE,
                                backup::INTERACTIVE_YES, backup::MODE_IMPORT, $admin->id);
    $backupid = $bc->get_backupid();
    $bc->get_plan()->get_setting('users')->set_status(backup_setting::LOCKED_BY_CONFIG);
    
    foreach ($settings as $setting => $configsetting) {
        if ($bc->get_plan()->setting_exists($setting)) {
            $bc->get_plan()->get_setting($setting)->set_value($config->{$configsetting});
        }
    }

    // backing up
    $bc->finish_ui();
    $bc->execute_plan();
    $bc->destroy();
    unset($bc);

    return $backupid;
}


//function to restore to course
function restore_to_course($courseid, $backupid) {
    global $CFG;
    raise_memory_limit(MEMORY_EXTRA);
	$admin = get_admin();
	$config = get_config('backup');
    $tempdestination = $CFG->tempdir . '/backup/' . $backupid;
    if (!file_exists($tempdestination) || !is_dir($tempdestination)) {
        print_error('unknownbackupexporterror'); // shouldn't   happen ever
    }

    $rc = new restore_controller($backupid, $courseid, backup::INTERACTIVE_YES, 
                        backup::MODE_IMPORT, $admin->id, true);

    // Convert the backup if required.... it should NEVER happed
    if ($rc->get_status() == backup::STATUS_REQUIRE_CONV) {
        $rc->convert();
    }
    // Mark the UI finished.
    $rc->finish_ui();
    // Execute prechecks
    $rc->execute_precheck();
    
    if (backup::TARGET_CURRENT_DELETING == 1 || backup::TARGET_EXISTING_DELETING == 1) {
        restore_dbops::delete_course_content($courseid);
    }
    // Execute the restore.
    $rc->execute_plan();
    $rc->destroy();
    unset($rc);
    
    // Delete the temp directory now
    fulldelete($tempdestination);
}

//update
function updatecourse($courseid, $categorypath,$coursefullname,$courseshortname,$startdate = null, $enddate = null){
	global $DB, $CFG;

    $course = new stdClass();
    $course->id = $courseid;
	$course->fullname = $coursefullname;
	$course->shortname = $courseshortname;
	$course->idnumber = $courseshortname;
    $course->category = $categorypath;
    $course->startdate = $startdate;
	try{
		$DB->update_record('course',$course);
	}catch(Exception $e){
        $event = \auth_anziif\event\course_restore::create(array(
            'objectid' => null,
            'courseid' => 1,
            'context' => context_system::instance(),
            'other' => array(
                'description' => $e->getMessage()
            )

        ));
        $event->trigger();
		mtrace($e->getMessage());
		return false;
    }
    return true;
}

//function send grade to ANZIIF crm api
function sendgrade($object,$courseidnumber,$masterid,$activityidnumber,$enrolment = null,$gradeitem){
	global $CFG, $DB;
    $anziifconfig = get_config('auth/anziif');
    $admin = get_admin();
    if($gradeitem->itemtype == 'mod'){
        mtrace('Sending activity grade');

         $requestParams = array(
             'assessmentResult' => array(
                 'MasterId' => $masterid,
                 'CourseId' => $courseidnumber,
                 'ActivityId' => $activityidnumber->idnumber,
                 'Result' => $object->other->finalgrade,
                 'ResultDate' => date('Y-m-d\TH:i:s.u', $object->timecreated)
             ),
             'wstoken' => $anziifconfig->crmtokenapi
         );
         $client = new SoapClient($anziifconfig->crmurlapi);
         try{
             $apiresponse = $client->SetAssessmentResult($requestParams);
         }catch(Exception $e){
            if($e->getMessage()){
                               
                $email = $admin;
                $email->maildisplay = true;
                $email->email = $anziifconfig->emailforerror;
                $supportuser = core_user::get_support_user();
                $noreplyaddressdefault = 'noreply@' . get_host_from_url($CFG->wwwroot);
                $noreplyaddress = empty($CFG->noreplyaddress) ? $noreplyaddressdefault : $CFG->noreplyaddress;
                $course = $DB->get_record('course',array('id' => $object->courseid));
                
                //send email notification
                $body = 'Affected User(Master ID): '.$masterid.'<br/>';
                $body .= 'Course Shortname: '.$course->shortname.'<br/>';
                $body .= 'Course ID: '.$object->courseid.'<br/>';
                $body .= 'Activity Name: '.$gradeitem->itemname.'<br/>';
                $body .= 'Activity ID number: '.$activityidnumber->idnumber.'<br/>';
                $body .= 'Result: '.$object->other->finalgrade.'<br/>';
                $body .= 'Result Date: '.date('Y-m-d\TH:i:s.u', $object->timecreated).'<br/>';
                $body .= 'Exception: '.$e->getMessage();
                $status = email_to_user($email,$noreplyaddress,'Anziiff Webservice Error Log!', $body,$body);
                $event = \auth_anziif\event\sending_grade::create(array(
                    'objectid' => $object->objectid,
                    'courseid' => $object->courseid,
                    'context' => context_module::instance($activityidnumber->id),
                    'relateduserid' => $object->relateduserid,
                    'other' => array(
                        'description' => $body
                    )
        
                ));
                $event->trigger();
                var_dump($body);

                $gg = $DB->get_record_sql("SELECT * FROM mdl_grade_items WHERE courseid = ? AND id = ?",array($object->courseid,$gradeitem->id)); 
                $coursemodule = $DB->get_record_sql("SELECT CM.* FROM mdl_course_modules CM JOIN mdl_modules M ON M.id = CM.module WHERE CM.instance = ? AND CM.course = ? AND M.name = ?",array($gg->iteminstance, $object->courseid, $gg->itemmodule));
                $user = $DB->get_record('user',array('id'=> (int) $object->relateduserid));
                if($activityidnumber->idnumber == NULL){
                    $task = new \auth_anziif\task\send_grade();
                    $task->set_custom_data(array(
                        'objectdata' => $object,
                        'courseidnumber' => $courseidnumber,
                        'masterid' => $user->username,
                        'activityidnumber' => $coursemodule,
                        'enrolment'=>'111',
                        'grade_item' =>$gg   
                    ));
                    \core\task\manager::queue_adhoc_task($task);
                    mtrace('Adhoc task will repeat');
                }
                
                return true;
            }

         }
         
         $event = \auth_anziif\event\sending_grade::create(array(
            'objectid' => $object->objectid,
            'courseid' => $object->courseid,
            'context' => context_module::instance($activityidnumber->id),
            'relateduserid' => $object->relateduserid,
            'other' => array(
                'description' => 'Grade successfully sent to CRM!'
            )

        ));
        $event->trigger();
        mtrace('Grade successfully sent to CRM!');
        return true;
        
    }else{
        mtrace('I dont have any work to do! :)');
    }
    
}